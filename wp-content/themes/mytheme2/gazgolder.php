<?php
/*
Template Name: gazgolder
*/
?>

<?php include 'header.php' ; ?>  
    
<section id="video-forma">
		<div class="row">
      <div class="col-md-7 col-sm-12 col-xs-12">
      <div class="tv_container ">
        <iframe class="videof" src="https://www.youtube.com/embed/DYFscRo4Yq0" frameborder="0" allowfullscreen></iframe>
      </div> 
			</div>
			<div class="col-md-5 col-sm-12 col-xs-12">
				<div class="dushbord__form">
					<h2 class="dushbord__form_header">Вызовите мастера</h2>
					<form action="" method="post">
						<label for="name" class="dushbord__form_label">Ваше имя</label><br>
						<input type="text" name="name" id="name" class="dushbord__form_input" required="yes"><br>
						<label for="email" class="dushbord__form_label">Ваш e-mail</label><br>
						<input type="email" name="e-mail" id="email" class="dushbord__form_input" required="yes"><br>
						<label for="phone" class="dushbord__form_label">Ваш телефон</label><br>
						<input type="tel" name="phone" id="phone" class="dushbord__form_input" required="yes"><br>
						<input type="submit" name="submit" value="ОТПРАВИТЬ" class="dushbord__form_submit">
					</form>
									</div>
			</div>
		</div>
	</section>
  <section id="mini_faq">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="collapsed">1. Безопасно ли газовое отопление?</a>
          </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" style="height: 0px;">
          <div class="panel-body">
            <p>
              Система автономной газификации является абсолютно безопасной и невзрывоопасной. Во-первых, на газгольдере имеется сбросные клапана, которые при увеличении давления сбрасывают газ, что не дает газгольдеру взорваться.
      Во вторых, в помещениях где установлено газовое оборудование нами устанавливаются автоматические газоанализаторы с сигнализатором утечки и отсекающем клапанном, что позволяет избежать утечки газа.
 
            </p>
            <ul>
              <li>
                Газонакопительная станция ТОО «LPG Дистрибьюшн», использует высокотехнологичное газовое оборудование.
                <p></p>
              </li>
              <li>
                В настоящее время станция насчитывает 200 сотрудников, имеющих высокую профессиональную подготовку и многолетний опыт работы в системе газоснабжения крупных городов и населенных пунктов Республики Казахстан.
              </li>
              <li>
                Станция обеспечивает потребителей автогазом, баллонным и емкостным газом для коммунально-бытового потребления.
              </li>
              <li>
                Продаваемому на станции газу присвоена торговая марка «Жігергаз».
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">2. Гарантия качества газа</a>
          </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" style="height: 0px;">
          <div class="panel-body">
            <p>
              Собственная аттестованная производственная
              <a href="/attestovannaya-proizvodstvennaya-laboratoriya-/">лаборатория</a>
              постоянно контролирует состав сжиженного газа на соответствие ГОСТ 20448-90, с помощью газового хроматографа «Кристалл – 2000М».
            </p>
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">3. Сколько стоит заправка газгольдера</a>
          </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" style="height: 0px;">
          <div class="panel-body">
            Стоимость газа зависит от отпускной цены на газонаполнительных станциях и определяется качеством газа и накладными расходами на его доставку. Поскольку удельные накладные расходы (в расчете на 1 литр перевозимого газа) у небольшого газовоза гораздо больше, чем у его большого собрата, цена газа за литр в системе автономной газификации получается практически такой-же, как цена на газовой заправке.

  Более низкая цена свидетельствует о том, что это либо нестандартный газ с большим содержанием бутана, либо отопительный газ, предназначенный для систем с испарителями и не подходящий для бытовых систем автономной газификации. Такого газа следует избегать.

  Долго экономить на качестве газа не выйдет. Рано или поздно снабжение некачественным газом приведёт к тому, что потребуются дорогостоящие процедуры по откачке и утилизации конденсата.
            <p>Заявки по телефону: 8(7172) 97 83 83; 8 701 9 901 888</p>
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapseThree">3. Аварийно-диспетчерская служба</a>
          </h4>
        </div>
        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" style="height: 0px;">
          <div class="panel-body">
            <ul>
              <li>
                Первоначальные меры по локализации и ликвидации аварийных ситуаций
              </li>
              <li>Круглосуточный приём заявок</li>
              <li>Гарантия 3 месяца на все виды работ</li>
            </ul>
            <p>Заявки по телефону: 8(7172) 97 83 83; 8 701 9 901 888</p>
          </div>
        </div>
      </div>
            <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapseThree">3. Аварийно-диспетчерская служба</a>
          </h4>
        </div>
        <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" style="height: 0px;">
          <div class="panel-body">
            <ul>
              <li>
                Первоначальные меры по локализации и ликвидации аварийных ситуаций
              </li>
              <li>Круглосуточный приём заявок</li>
              <li>Гарантия 3 месяца на все виды работ</li>
            </ul>
            <p>Заявки по телефону: 8(7172) 97 83 83; 8 701 9 901 888</p>
          </div>
        </div>
      </div>
    </div>
  </section>
    <section id="compare"><div class="container">
  <h2>Сравнительная Таблица материалов для автономного Газообеспечения</h2>
  <p>Не знаете чем топить свой дом? Эта таблица поможет вам сделать правильный выбор</p>            
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Материал</th>
        <th>КПД</th>
        <th>Экологичность</th>
        <th>Срок службы</th>
        <th>Стоимость</th>
        <th>Общая оценка</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Бензин</td>
        <td>87%</td>
        <td>6</td>
        <td>3 года</td>
        <td>170000тг</td>
        <td>7</td>
      </tr>
      <tr>
        <td>Мазут</td>
        <td>69%</td>
        <td>4</td>
        <td>3 года</td>
        <td>150000тг</td>
        <td>5</td>
      </tr>
      <tr class="l-blue inverse">
        <td>Газ</td>
        <td>84%</td>
        <td>9</td>
        <td>5 лет</td>
        <td>130000тг</td>
        <td>9</td>
      </tr>
      <tr>
        <td>Электричество</td>
        <td>72%</td>
        <td>10</td>
 		<td>10 лет</td>
        <td>260000тг</td>
        <td>8</td>     
      </tr>
      <tr>
        <td>Уголь/дрова</td>
        <td>46%</td>
        <td>6</td>
        <td>20 лет</td>
        <td>45000тг</td>
        <td>8</td>
      </tr>
    </tbody>
  </table>
</div></section>

        
<?php  require 'section/sertif.php' ;
  require  'footer.php' ; ?>  