<style>.card-1 {
	background: #fff;
    border-radius: 2px;
    display: inline-block;
    min-height: 400px;
    max-height: 470px;
    margin: 1rem;
    position: relative;
    min-width: 180px;
    max-width: 400px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
    transition: all 0.3s cubic-bezier(.25,.8,.25,1);}
.card-1:hover {box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);}
.i-card {width: 100%; height: 200px;}
@media (min-width: 992px; max-width: ) {
	
}
</style>
<section id="services">
	<h2>Услуги компании ГорГаз</h2>
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="card-1"><img class="i-card" src="/img/img_av.png" alt="">
				<div class="card_title">
				<h3>Доставка газа</h3>
				</div>
				<p>Круглосуточная БЕСПЛАТНАЯ доставка газа. 8кг 800 тг 10кг 1000 тг 20кг 2000 тг. Доставляем за час! Заправка, обмен, доставка газовых баллонов с </p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="card-1"><img class="i-card" src="/img/img_av.png" alt="">
				<div class="card_title">
				<h3>Газовые заправки</h3>
				</div>
				<p>Адреса ближайших газовых заправок на карте. 45 тг литр. Заправляйтесь у нас. Скидочные карты для постоянных клиентов. </p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="card-1"><img class="i-card" src="/img/img_av.png" alt="">
				<div class="card_title">
				<h3>Автономная газификация</h3>
				</div>
				<p>Установка газгольдеров и газовых котлов для отопления. Сжиженный газ для газгольдеров. Отопление, горячая вода и даже теплые полы на газу. </p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="card-1"><img class="i-card" src="/img/img_av.png" alt="">
				<div class="card_title">
				<h3>Установка ГБО</h3>
				</div>
				<p>Экономьте на бензине до 150000 тенге в год. Переводите свой автомобиль на газ. Дешевое и экологическое топливо, низкие вибрации, шум и износ двигателя. </p>
				</div>
			</div>
		</div>
	</section>