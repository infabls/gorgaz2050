<!DOCTYPE html>
<html lang="ru">
<head>
	<?php wp_head(); ?>
	<meta charset="UTF-8">
	<title></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="/wp-content/themes/mytheme/css/libs.min.css">
	<link rel="stylesheet" href="/wp-content/themes/mytheme/base.css">
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" href="/wp-content/themes/mytheme/head.css">
	


	<!-- <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/jasny-bootstrap.css">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/font-awesome.min.css"> -->
	<script>
var h_hght = 90; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
(function(){
 
    var elem = $('#top_nav');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css('top', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css('top', (h_hght-top));
        } else {
            elem.css('top', h_mrg);
        }
    });
 
});
</script>
</head>
<div class="wrapper">
<body data-spy="scroll">
<div>
<button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas="body"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button></div>
<header>

<!-- <div id="top" class="inverse">
<div class="row">
	<div class="col-sm-6 col-xs-12 col-md-4">
		<label for="city">Выберите ваш город</label>
		<select name="city" id="city">
			<option value="">Астана</option>
			<option value="">Караганда</option>
			<option value="">Павлодар</option>
			<option value="">Усть-Каменогорск</option>
		</select>
	</div>
	<div class="col-sm-6 col-xs-12 col-md-4">
		<h5>Наш адрес: Космополитическая 11/2</h5>
	</div>
	<div class="col-sm-6 col-xs-12 col-md-4">
		<h5>Позвоните нам +77777 90 80 40</h5>
	</div>
</div>
</div>
-->
<div class="row  hidden-xs">
<div class="topper">
	<div class="col-md-3 col-sm-3">
		<a href="index.php">
			<div class="topper__logo"></div>
		</a>
	</div>
	<div class="col-md-6 col-md-offset-3 col-sm-9">
		<div class="topper__contact">
			<div class="topper__address-icon"></div>
			<p> <strong>г. Усть-Каменогорск,
					<br>ул. Чистякова, 10</strong>
			</p>
		</div>
		<div class="topper__contact">
			<div class="topper__phone-icon"></div>
			<p> <strong><a href="tel:+77232221568">+7 (7232) 50-31-68</a>
					<br>
					<a href="tel:+77054012344">+77777 90-80-40</a></strong> 
			</p>
		</div>
	</div>
</div>
</div>
<nav id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-left offcanvas" role="navigation">
<a class="navmenu-brand" href="#">TOO "Горгаз 2050"</a>
<?php wp_nav_menu( array( 'container' => '', 'theme_location' => 'mobile' ) ); ?>
<ul class="nav navmenu-nav">
	<li class="active">
		<a href="index.php">Главная</a>
	</li>
	<li>
		<a href="avtogaz.php">Газовые заправки</a>
	</li>
	<li>
		<a href="ustanovka-gbo.php">Газобалонное оборудование</a>
	</li>
	<li>
		<a href="dostavka-gaza.php">Доставка газа</a>
	</li>
	<li>
		<a href="gazgolder.php">Газ для газгольдеров</a>
	</li>
	<li>
		<a href="faq-2.php">Ответы на ваши вопросы</a>
	</li>
	<li class="hidden-sm hidden-md">
		<a href="/blog">Блог</a>
	</li>
	<li class="hidden-sm hidden-md">
		<a href="/reviews">Отзывы</a>
	</li>
	<li class="hidden-sm">
		<a href="/contacts">Контакты</a>
	</li>
</ul>
</nav>
<nav id="top_nav" class="navbar hidden-xs" >
<div id="navbar" class="navbar-collapse menu-collapse menu-shadow collapse in" aria-expanded="true">
	
	<ul class="nav navbar-nav">
		<?php wp_nav_menu( array( 'items_wrap' => '%3$s', 'container_class' => 'nav navbar-nav', 'theme_location' => 'primary' ) ); ?>
		</li>
	</ul>
</div>
</nav>
</header>
 <main>
