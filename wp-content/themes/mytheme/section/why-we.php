
<style>
	.bg-w {width: 128px; height: 128px; margin: auto;}
	.bg-shield {background: url('/img/why-we.png') -281px -5px}
	.bg-dog {background: url('/img/why-we.png') -5px -5px}
	.bg-sert {background: url('/img/why-we.png') -143px -143px}
	.bg-medal {background: url('/img/why-we.png') -5px -143px}
	.bg-fuel {background: url('/img/why-we.png') -143px -5px}
	.bg-spec {background: url('/img/why-we.png') -281px -143px}

</style>

<section id="why-we">
		<div class="container">
			<div class="row">
				<h2>Почему клиенты выбирают нас?</h2>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="bg-w bg-shield"></div>
					<h3>Защита 100%</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit cupiditate error eveniet? Voluptatum quaerat sit odit nisi harum, eos impedit. Labore optio repudiandae quos, reprehenderit! Temporibus iusto minus, possimus asperiores.</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="bg-w bg-dog"></div>
					<h3>Работаем по договору</h3>
					<p>Id quo quasi perspiciatis aliquam dicta autem repellendus, asperiores consectetur porro. Hic consectetur laudantium eius vero molestias dolore quo explicabo consequuntur, labore unde esse necessitatibus architecto ex, quibusdam veniam minima.</p>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="bg-w bg-sert"></div>
					<h3>Гарантия качества</h3>
					<p>Labore exercitationem dolor praesentium excepturi totam aliquam illo quas aliquid assumenda maiores nisi iste modi, quibusdam ipsum ipsam deserunt, hic tempora aut nesciunt quis recusandae eveniet. Alias neque, voluptas? Dignissimos.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="bg-w bg-medal"></div>
					<h3>Лидеры рынка</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita, optio repellendus odit asperiores animi quaerat, adipisci similique ex facere saepe. Neque, consequatur doloremque! Sunt perferendis natus laudantium quasi, ducimus odit!</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="bg-w bg-fuel"></div>
					<h3>Лучшее топливо</h3>
					<p>Facere animi, voluptates perferendis? Rerum id in, fugiat suscipit sed ratione possimus necessitatibus. Cumque atque quia sapiente deleniti maiores explicabo adipisci officia hic voluptatibus, distinctio, voluptatem, error eos illo! Pariatur.</p>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="bg-w bg-spec"></div>
					<h3>Приём заявок 24/7</h3>
					<p>Quas ipsam ratione neque quam, atque blanditiis voluptatem vel incidunt dolorem vitae, recusandae voluptates amet deleniti eligendi a natus ipsa quia! Rem rerum eveniet fuga suscipit odit vitae possimus nesciunt?</p>
				</div>
			</div>
		</div>
	</section>