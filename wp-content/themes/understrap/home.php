<?php
/*
Template Name: Home
*/
?>

<?php include 'header.php';?>
  <?php require 'section/servise.php'; 
   require 'section/why-we.php'; 
   require 'section/0%.php';
   ?>



  <section id="mini_faq">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="collapsed">1. Современная газонаполнительная станция</a>
          </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" style="height: 0px;">
          <div class="panel-body">
            <p>
              Для обеспечения населения города Астаны и Акмолинской области сжиженным нефтяным газом ТОО «LPG Дистрибьюшн» с торговой маркой «Жігергаз»™, в 2008 году запустила в эксплуатацию газонаполнительную станцию мощностью 24 000 тонн в год, и резервуарный парк на 10 ёмкостей, с единовременным объёмом хранения 2 000 куб.м. Мы гарантируем своим клиентам бесперебойную поставку газа.
            </p>
            <ul>
              <li>
                Газонакопительная станция ТОО «LPG Дистрибьюшн», использует высокотехнологичное газовое оборудование.
                <p></p>
              </li>
              <li>
                В настоящее время станция насчитывает 200 сотрудников, имеющих высокую профессиональную подготовку и многолетний опыт работы в системе газоснабжения крупных городов и населенных пунктов Республики Казахстан.
              </li>
              <li>
                Станция обеспечивает потребителей автогазом, баллонным и емкостным газом для коммунально-бытового потребления.
              </li>
              <li>
                Продаваемому на станции газу присвоена торговая марка «Жігергаз».
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">2. Гарантия качества газа</a>
          </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" style="height: 0px;">
          <div class="panel-body">
            <p>
              Собственная аттестованная производственная
              <a href="/attestovannaya-proizvodstvennaya-laboratoriya-/">лаборатория</a>
              постоянно контролирует состав сжиженного газа на соответствие ГОСТ 20448-90, с помощью газового хроматографа «Кристалл – 2000М».
            </p>
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">3. Аварийно-диспетчерская служба</a>
          </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" style="height: 0px;">
          <div class="panel-body">
            <ul>
              <li>
                Первоначальные меры по локализации и ликвидации аварийных ситуаций
              </li>
              <li>Круглосуточный приём заявок</li>
              <li>Гарантия 3 месяца на все виды работ</li>
            </ul>
            <p>Заявки по телефону: 8(7172) 97 83 83; 8 701 9 901 888</p>
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapseThree">3. Аварийно-диспетчерская служба</a>
          </h4>
        </div>
        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" style="height: 0px;">
          <div class="panel-body">
            <ul>
              <li>
                Первоначальные меры по локализации и ликвидации аварийных ситуаций
              </li>
              <li>Круглосуточный приём заявок</li>
              <li>Гарантия 3 месяца на все виды работ</li>
            </ul>
            <p>Заявки по телефону: 8(7172) 97 83 83; 8 701 9 901 888</p>
          </div>
        </div>
      </div>
            <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapseThree">3. Аварийно-диспетчерская служба</a>
          </h4>
        </div>
        <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" style="height: 0px;">
          <div class="panel-body">
            <ul>
              <li>
                Первоначальные меры по локализации и ликвидации аварийных ситуаций
              </li>
              <li>Круглосуточный приём заявок</li>
              <li>Гарантия 3 месяца на все виды работ</li>
            </ul>
            <p>Заявки по телефону: 8(7172) 97 83 83; 8 701 9 901 888</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php include'section/sertif.php' ?>
<?php include 'footer.php';?>