<?php
/*
Template Name: Ustanovka-gbo
*/
?>

<?php include 'header.php' ; ?>  
    <style>#video{position: relative;
    display: block;
    height: 0;
    padding: 0;
    overflow: hidden;} @media (max-width: 992px){
.dushbord__form {
    width: 80%;
    margin: 9px auto 9px;
	    margin-top: 10px;}	
}</style>
  	<section id="video-forma">
		<div class="row">
			<div class="col-md-7 col-sm-12 col-xs-12">
				<div class="tv_container ">

				<iframe class="videof" src="https://www.youtube.com/embed/DYFscRo4Yq0" frameborder="0" allowfullscreen></iframe>
			</div> 
			</div>
			<div class="col-md-5 col-sm-12 col-xs-12">
				<div class="dushbord__form">
					<h2 class="dushbord__form_header">Закажите установку ГБО</h2>
					<form action="" method="post">
						<label for="name" class="dushbord__form_label">Ваше имя</label><br>
						<input type="text" name="name" id="name" class="dushbord__form_input" requivar(--red)="yes"><br>
						<label for="email" class="dushbord__form_label">Ваш e-mail</label><br>
						<input type="email" name="e-mail" id="email" class="dushbord__form_input" requivar(--red)="yes"><br>
						<label for="phone" class="dushbord__form_label">Ваш телефон</label><br>
						<input type="tel" name="phone" id="phone" class="dushbord__form_input" requivar(--red)="yes"><br>
						<input type="submit" name="submit" value="ОТПРАВИТЬ" class="dushbord__form_submit">
					</form>
									</div>
			</div>
		</div>
	</section>
	<section id="versus">
		<div class="versus_img" style="height: 1000px; background: url(img/gvb.jpg)  no-repeat;"></div>	
	</section>
<script language="javascript">

function calc(form) {
var GAZ = document.form.GAZ.value.replace(',', '.');
var BENZIN = document.form.BENZIN.value.replace(',', '.');
var ROZHOD = document.form.ROZHOD.value.replace(',', '.');
var PROBEG = document.form.PROBEG.value.replace(',', '.');

var DAY = (((BENZIN-GAZ)*ROZHOD*PROBEG)/100);

document.form.DAY.value = Math.round(DAY);
document.form.EKONOM.value = Math.round(DAY*365);
}

</script>

<section id="calcgbo">
<!-- <div class="padding" style="margin-top:100px; "></div> -->
		<div class="row">
			<div class="col-md-7">
	<!-- <td >&nbsp; -->
	<div class="cform">

<span class="cform-txt"><h1 style="padding-bottom: 0px;margin:0;color: #fff;">Калькулятор окупаемости ГБО:</h1></span>
<form name="form" onsubmit="calc(this)" method="post">
<div class="col-md-12">
<table  style="width: 100%;" cellspacing="0" cellpadding="5" align="left" border="0">
<tbody>
<tr>
<td  style="background: var(--red);padding-left:0;"  ><span class="cform-txt">Средний расход бензина на 100 км пробега:</span></td >
<td style="background: var(--red);padding-left:0;" ><input class="dushbord__form_input-cform" size="7" value="30" name="ROZHOD"><span class="cform-txt"> л </span></td ></tr>
<tr>
<td style="background: var(--blue);padding-left:0;" ><span class="cform-txt">Средний суточный пробег Вашего автомобиля:</span></td >
<td style="background: var(--blue);padding-left:0;" ><input class="dushbord__form_input-cform" size="7" value="100" name="PROBEG"><span class="cform-txt"> км</span></td ></tr>
<tr>
<td style="background: var(--red);padding-left:0;" ><span class="cform-txt">Экономия за Год:</span></td >
<td style="background: var(--red);padding-left:0;" ><input class="dushbord__form_input-cform" size="7" value="0" name="EKONOM"> <span class="cform-txt"> тг</span></td ></tr>
<tr>
<td style="background: var(--blue);padding-left:0;"><span class="cform-txt">Окупаемость в днях:</span></td >
<td style="background: var(--blue);padding-left:0;"><input class="dushbord__form_input-cform" size="7" value="0" name="DAY"><span>&nbsp&nbsp&nbsp&nbsp</span></td ></tr>
</tbody>
</table>
</div>
</div>
<!-- </td > -->
</div>			
			<div class="col-md-5 col-sm-6">
				<div class="dushbord__form"  id="position_form">
					<h2 class="dushbord__form_header-cform">Цены</h2>
					<label for="GAZ" class="dushbord__form_label">Цена 1 литра газа:</label><br>
						<input class="dushbord__form_input" size="7" value="45" name="GAZ"><br>
						<label for="BENZIN" class="dushbord__form_label">Цена 1 литра бензина</label><br>
						<input class="dushbord__form_input" size="7" value="145" name="BENZIN"><br>
						<input type="button" onclick="calc(this)" name="button" value="Подсчитать" class="dushbord__form_submit">
					</form>
				</div>
			</div>
		</div>
	</section>      
 

<?php require 'section/sertif.php';        
 require 'section/map.php';
require 'footer.php' ; ?>  